package br.com.itau;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class Main {
    public static void main(String[] args) throws IOException {
        InputStreamReader isr = new InputStreamReader(System.in);
        BufferedReader br = new BufferedReader(isr);
        Integer totalhumano = 0;
        Integer totalcomputador = 0;
        String teclado="";

        System.out.println("Deseja iniciar um novo jogo? [i] Iniciar e [f] Finalizar");
        teclado = br.readLine();
        while ((teclado.equalsIgnoreCase("i")) == true) {
                // reset variaveis

            Baralho d = new Baralho();
            d.criarCartas();
            Maos maoComputador = new Maos();
            Maos maoHumano = new Maos();
                //distribuir cartas
                maoComputador.adicionarCarta(d.pegarCarta());
                maoComputador.adicionarCarta(d.pegarCarta());

                maoHumano.adicionarCarta(d.pegarCarta());
                maoHumano.adicionarCarta(d.pegarCarta());

                //se tem 16 ou menos computador pega uma carta
                while (maoComputador.getValor() <= 16) {
                    maoComputador.adicionarCarta(d.pegarCarta());
                }

                System.out.println("Suas Cartas: " + maoHumano.toString());
                System.out.println("Total de Pontos: " + maoHumano.getValor());
                System.out.println("Deseja outra carta ou não? [s] Sim e [n]");

                teclado = br.readLine();
                while ((teclado.equalsIgnoreCase("s")) == true) {
                        maoHumano.adicionarCarta(d.pegarCarta());
                        System.out.println("Suas Cartas são: " + maoHumano.toString());
                        System.out.println("Total de Pontos: " + maoHumano.getValor());
                        System.out.println("Deseja outra carta ou não? [s] Sim e [n]");
                        teclado = br.readLine();
                }
                System.out.println("Cartas do computador: " + maoComputador.toString());
                if (maoComputador.getValor() < maoHumano.getValor()) {
                    totalhumano = totalhumano +1;
                    System.out.println("Você venceu!");
                    System.out.println("Você: " + maoHumano.getValor());
                    System.out.println("Computador: " + maoComputador.getValor());
                    System.out.println("Total Computador: " + totalcomputador);
                    System.out.println("Total Humano " + totalhumano);
                    System.out.println("Deseja iniciar um novo jogo? [i] Iniciar e [f] Finalizar");
                    teclado = br.readLine();
                } else if (maoComputador.getValor() == maoHumano.getValor()) {
                    System.out.println("Empate!");
                    System.out.println("Você: " + maoHumano.getValor());
                    System.out.println("Computador: " + maoComputador.getValor());
                    System.out.println("Total Computador: " + totalcomputador);
                    System.out.println("Total Humano " + totalhumano);
                    System.out.println("Deseja iniciar um novo jogo? [i] Iniciar e [f] Finalizar");
                    teclado = br.readLine();
                } else if (maoComputador.getValor() > maoHumano.getValor()) {
                    totalcomputador = totalcomputador+1;
                    System.out.println("Computador venceu!");
                    System.out.println("Você: " + maoHumano.getValor());
                    System.out.println("Computador: " + maoComputador.getValor());
                    System.out.println("Total Computador: " + totalcomputador);
                    System.out.println("Total Humano " + totalhumano);
                    System.out.println("Deseja iniciar um novo jogo? [i] Iniciar e [f] Finalizar");
                    teclado = br.readLine();
                }
            }
                System.out.println("Saiu do Jogo!");
    }
}
