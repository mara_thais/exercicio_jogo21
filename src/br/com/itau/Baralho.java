package br.com.itau;

import java.util.Arrays;
import java.util.Collections;

public class Baralho {
    Carta cartas[] = new Carta[52];
    int posicao;

    //52 cartas e 4 tipos/naipes
    public void criarCartas() {
        String simbolos = "23456789TJQKA";
        String tipos = "EPOC";
        int index = 0;

        //gerar as 52 cartas do baralho e associar
        for (int i = 0; i < tipos.length(); i++) {
            for (int j = 0; j < simbolos.length(); j++) {
                Carta tempCarta = new Carta();
                tempCarta.setSimbolo(simbolos.charAt(j));
                tempCarta.setTipo(tipos.charAt(i));
                this.cartas[index] = tempCarta;
                index++;
            }
        }

        embaralharCartas(); //embaralhar as cartas
    }

    //Metodo privado para embaralhar cartas
    private void embaralharCartas() {
        Collections.shuffle(Arrays.asList(this.cartas));
    }

    //Metodo publico que retorna a carta que está no topo do baralho
    public Carta pegarCarta() {
        if (posicao==51) {
            System.out.println("Não tem mais cartas para distribuir!");
            return null;
        }
        Carta tempCarta = this.cartas[posicao];
        posicao++; //move uma posição para a direita
        return tempCarta;
    }
}


