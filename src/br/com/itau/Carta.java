package br.com.itau;

public class Carta {
    private char simbolo; //23456789TJQKA
    private char tipo; //EPOC - Espada - Paus - Ouros - Copas

    //Defini o simbolo para a carta.
    public void setSimbolo(char s) {
        this.simbolo = s;
    }

    //Defini o tipo/naipe para a carta.
    public void setTipo(char t) {
        this.tipo = t;
    }

    //Retorna o simbolo da carta.
    public char getSimbolo() {
        return this.simbolo;
    }

    //Retorna o tipo da carta.
    public char getTipo() {
        return this.tipo;
    }

    //Retorna o valor númerico da carta
    public int getValor() {
        if (this.simbolo=='T') return 10;
        else if (this.simbolo=='J') return 11;
        else if (this.simbolo=='Q') return 12;
        else if (this.simbolo=='K') return 13;
        else if (this.simbolo=='A') return 1;
        else return Integer.parseInt(this.simbolo+""); //Para usar como string
    }

    //Retorna a carta no formato AC (As de copas), 2P (2 de paus) e 8E (8 de espada)
    public String toString(){
        return this.simbolo+""+this.tipo;
    }
}

