package br.com.itau;

import java.util.ArrayList;

public class Maos {
    ArrayList<Carta> arr = new ArrayList<Carta>();
    int posicao = 0;

    //Adiciona uma carta na mao
    public void adicionarCarta(Carta c) {
        arr.add(c);
    }

    //Devolve para a mao o formato da carta
    public String toString(){
        String mao ="";
        for (int i=0; i < arr.size(); i++)
            mao += arr.get(i).toString() + " ";
        return  mao;
    }

    //Retorna para a mao o valor numerico
    public  int getValor() {
        //aqui pode valer de 1 a 11
        int val =0; //valor computado em maos
        boolean ace=false; //verdadeiro se na mão tem A

        for (int i=0; i < arr.size(); i++) {
            Carta tmpCarta = arr.get(i);
            int cartaValor = tmpCarta.getValor();
            if (cartaValor>10) //é um J,Q,K
                cartaValor=10;
            if (cartaValor==1) //é um A
                ace = true;
            val += cartaValor;
        }
        // Se tem um A trocamos o valor de 1 e 11
        //menor igual a 21, entao nesse caso soma 10
        if (ace == true && val + 10 <= 21)
            val = val + 10;
        return val;
    }
}
